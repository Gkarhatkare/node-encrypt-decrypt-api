require('dotenv').config()
console.log(process.env);
console.log('::::::::::::;',process.env.LOCAL_PORT);
 const config = {
    localHost: process.env.LOCAL_PORT,
    database: process.env.DB_DATABASE,
    user :process.env.DB_USER,
    host :process.env.DB_HOST,
    password: process.env.DB_PASSWORD,
    port : process.env.DB_PORT,
    
}
module.exports = config