const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fs = require('fs');
const moment = require('moment')
const multer = require('multer')
const path = require('path')
const userRoutes = require('../routes/user.route')
const app = express(); 
console.log('express file call');   
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cors());
console.log("express file");
// app.use('/api',()=>{
//     console.log("api");

// }, userRoutes);

// const uploadFile = multer({
//     storage : multer.diskStorage({
//         destination: function(req,file,callback){
//             callback(null,"assets");
//         },
//         filename:function(req,file,callback){
//             const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
//             callback(null, file.fieldname + '-' + uniqueSuffix)
//         }
//     })
// })




app.use('/api', userRoutes);    

module.exports = app;
