const {Client} = require('pg')
const config = require('./config')
console.log('*****',config.host);
const client = new Client({
    host : config.host,
    user:config.user,
    database:config.database,
    password:config.password,
    port:config.port
})
client.connect(function(err){
    if(err) throw err;
    console.log('Connect');
})