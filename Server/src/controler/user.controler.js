const { v4: uuidv4 } = require("uuid");
var CryptoJS = require("crypto-js");

const userControler = () => {
  const userDataContainer = [];

  // console.log("userDataContainer", userDataContainer);
  const _createUser = async (req, resp) => {
    // console.log("create user call");
    let userData = req.body;

    const userId = uuidv4();
    // userData = {...userData , id : userId };
    userDataContainer.push({ ...userData, id: userId });
    // userDataContainer.push(userData);
    // console.log(userData);
    resp.send(" user created");
    // try{

    // }
    // catch{

    // }
  };
  const _getUser = async (req, resp) => {
    console.log("get user call");
    resp.send(userDataContainer);
  };
  const _updateUser = async (req, resp) => {
    const { id } = req.params;
    const data = req.body;
    // console.log("iddddddd", id);
    // console.log("data", data);

    const updatedData = userDataContainer.find((user) => user.id === id);
    const parseData = JSON.parse(updatedData);
    console.log("****************", parseData);
    resp.send(`user updated : ${updatedData}`);
  };
  const _deleteUser = async (req, resp) => {
    const { id } = req.params;
    const updatedData = userDataContainer.find((user) => user.id !== id);
    resp.send("user deleted");
  };



///////////////enc && dec///////////////////
const encryptFun = (text) => {
    // 3
    var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(text), 'secret key 123').toString();
    console.log('ciphertext',ciphertext);
    return ciphertext
}

const decryptFun = (ciphertext) => {
    console.log('hash:::::::::',ciphertext);
var bytes  = CryptoJS.AES.decrypt(ciphertext, 'secret key 123');
var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
console.log('decryptedData',decryptedData);
return decryptedData
}



const get_encrypt_data = async(req, resp) =>{
    const text = {
        username: "gauav.k@adixoo.com",
        password:"gaurav@123",
        age:25,
        name:"gaurav karhatkare"
      }
    const encryptedData = await encryptFun(text)
    console.log('data............',encryptedData);
    resp.send(encryptedData)
}

const post_encrypt_data = async(req, resp)=>{
        const data = req.body
        const keys = Object.keys(data);
        const encrptD = keys[0].replaceAll(' ','+');
        console.log('keys.length',keys[0].replaceAll(' ','+'));
        const decryptedData = await decryptFun(encrptD)

        console.log('decryptedData++++++++++++++',decryptedData);
}

  return {
    createUser: _createUser,
    getUser: _getUser,
    updateUser: _updateUser,
    deleteUser: _deleteUser,
    getEncryptData: get_encrypt_data,
    postEncryptData:post_encrypt_data
  };
};
module.exports = userControler();
