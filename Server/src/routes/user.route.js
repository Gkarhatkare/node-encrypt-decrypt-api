const userControler = require('../controler/user.controler');
const express = require('express');
const userRoute = express.Router();
console.log('route file call');
userRoute.route('/getuser').get(userControler.getUser);
userRoute.route('/createuser').post(userControler.createUser);
userRoute.route('/updateuser/:id').put(userControler.updateUser);
userRoute.route('/deleteuser/:id').delete(userControler.deleteUser);
userRoute.route('/encrypt-data').get(userControler.getEncryptData)
userRoute.route('/encrypt-data').post(userControler.postEncryptData)

module.exports=userRoute;